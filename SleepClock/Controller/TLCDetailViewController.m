//
//  TLCDetailViewController.m
//  SleepClock
//
//  Created by andy on 2016-11-19.
//
//

#import "TLCDetailViewController.h"

@interface TLCDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *alarmTimeLabel;
@property (weak, nonatomic) IBOutlet UIDatePicker *alarmDatePicker;
@property (weak, nonatomic) IBOutlet UISwitch *alarmTimeSwitch;

@property (strong,nonatomic) TLCUserDefaultManager *userDef;
@property (strong,nonatomic) TLCTimeManager *timeManager;

- (IBAction)onChangeDatePicker:(UIDatePicker *)sender;
- (IBAction)onAlarmSwitch:(UISwitch *)sender;

@property (weak, nonatomic) IBOutlet UIImageView *icoChaser;
@property (weak, nonatomic) IBOutlet UIImageView *icoAlarm;

@end

@implementation TLCDetailViewController

#pragma mark - functional contoroller method
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_alarmDatePicker setValue:[UIColor whiteColor] forKey:@"textColor"];
    
    CGRect defaultNavigationBarFrame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    NSArray *colors = [NSArray arrayWithObjects:FlatPurple, FlatBlackDark, nil];
    UIColor *gradientColor = [UIColor colorWithGradientStyle:UIGradientStyleTopToBottom
                                                   withFrame:defaultNavigationBarFrame
                                                   andColors:colors];
    self.view.backgroundColor = gradientColor;
    
    self.userDef = [TLCUserDefaultManager sharedManager];
    self.timeManager = [TLCTimeManager sharedManager];
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:0 animated:YES];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    self.alarmTimeSwitch.on = [self.userDef getDefaultAlarm];
    self.title=self.alarmTimeSwitch.on==YES?@"Alarm ON":@"Alarm OFF";
    
    
    [self setStateIcoAlarm];
    
    NSMutableDictionary *alarmTime = [self.userDef getDefaultAlarmTime];
    NSDate *createDate = [self.timeManager createDateFromHM:alarmTime[@"hour"] assginMinute:alarmTime[@"minute"]];
    [self.alarmDatePicker setDate:createDate];
    
    [self displayAlarmTimeToLabel];
    
}

- (IBAction)onChangeDatePicker:(UIDatePicker *)sender {
    if(self.alarmTimeSwitch.on){
        
        [self saveAlarmTime];
        [self displayAlarmTimeToLabel];
        
        [self clearAlarmTimer ];
        [self createAlarmTimer];
    }
    
}

- (IBAction)onAlarmSwitch:(UISwitch *)sender {
    if(sender.on){
        
        [self saveAlarmTime];
        [self displayAlarmTimeToLabel];
        
        [self clearAlarmTimer ];
        [self createAlarmTimer];
        [self.userDef setDefaultAlarm:YES];
        
        [self confirmNotificationAlert];
        
    } else {
        
        [self clearAlarmTimer];
        [self.userDef setDefaultAlarm:NO];
        [self.userDef setDisplayedAwakeAlarmView:NO];
    }
    
    [self setStateIcoAlarm];
}

-(void)createAlarmTimer{
    
    NSDate *trimDate = [self.timeManager createTrimDateFromNSDate:self.alarmDatePicker.date];
    
    TLCAlarmManager *alarmManager = [TLCAlarmManager sharedManager];
    [alarmManager setLocalNotification:[self.timeManager convertAlarmDate:trimDate]];
}

-(void)clearAlarmTimer{
    
    TLCAlarmManager *alarmManager = [TLCAlarmManager sharedManager];
    
    [alarmManager clearLocalNotification ];
}


- (void)saveAlarmTime{
    NSMutableDictionary *dict = [self.timeManager getAssignDate:self.alarmDatePicker.date];
    
    [ self.userDef setDefaultAlarmTime:[@{@"hour":dict[@"hour"],@"minute":dict[@"minute"]}mutableCopy] ];
}



#pragma mark - functonal view method

-(void)displayAlarmTimeToLabel{
    NSDictionary *alarmTimeObj = [self.userDef getDefaultAlarmTime];
    NSString *setStr = [NSString stringWithFormat:@"%@:%@",alarmTimeObj[@"hour"],alarmTimeObj[@"minute"]];
    
    self.alarmTimeLabel.text = setStr;
}

-(void)confirmNotificationAlert{
    
    if(NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1){
        return;
    }
    
    UIUserNotificationSettings *currentSettings = [[UIApplication
                                                    sharedApplication] currentUserNotificationSettings];
    
    if(currentSettings.types == UIUserNotificationTypeNone){
        
        UIAlertController * ac = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                     message:@"Please activate notification"
                                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              
                                                              
                                                          }];
        [ac addAction:okAction];
        
        [self presentViewController:ac animated:YES completion:nil];
    }
}


-(void)setStateIcoAlarm{
    if([[TLCUserDefaultManager sharedManager] getDefaultAlarm]){
        self.icoChaser.hidden = NO;
        self.icoAlarm.hidden = YES;
        
    } else {
        
        self.icoChaser.hidden = YES;
        self.icoAlarm.hidden = NO;
    }
    self.title=self.alarmTimeSwitch.on==YES?@"Alarm ON":@"Alarm OFF";
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
