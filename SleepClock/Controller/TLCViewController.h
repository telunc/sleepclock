//
//  TLCViewController.h
//  SleepClock
//
//  Created by andy on 2016-11-18.
//
//

#import <UIKit/UIKit.h>
#import "TLCDetailViewController.h"
#import "TLCUserDefaultManager.h"
#import "TLCRouter.h"
#import "AppDelegate.h"
#import <ChameleonFramework/Chameleon.h>
#import <PNChart.h>

@interface TLCViewController : UIViewController

@end
