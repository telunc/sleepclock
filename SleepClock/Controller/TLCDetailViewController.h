//
//  TLCDetailViewController.h
//  SleepClock
//
//  Created by andy on 2016-11-19.
//
//

#import <UIKit/UIKit.h>
#import "TLCUserDefaultManager.h"
#import "TLCTimeManager.h"
#import "TLCAlarmManager.h"
#import "TLCRouter.h"

@interface TLCDetailViewController : UIViewController

@end
