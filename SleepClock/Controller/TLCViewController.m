//
//  TLCViewController.m
//  SleepClock
//
//  Created by andy on 2016-11-18.
//
//

#import "TLCViewController.h"

@interface TLCViewController ()

@property (weak, nonatomic) IBOutlet UIButton *alarmTimeButton;
@property (weak, nonatomic) IBOutlet UIView *alarmTimeButtonWrapper;

@property (weak, nonatomic) IBOutlet UIImageView *ico_arrow;

- (IBAction)onTapToSettingButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;

@end

@implementation TLCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[TLCRouter sharedManager] displayAwakeAlarmView];
    
    CGRect defaultNavigationBarFrame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    NSArray *colors = [NSArray arrayWithObjects:FlatPurple, FlatBlackDark, nil];
    UIColor *gradientColor = [UIColor colorWithGradientStyle:UIGradientStyleTopToBottom
                                                   withFrame:defaultNavigationBarFrame
                                                   andColors:colors];
    self.alarmTimeButtonWrapper.backgroundColor = gradientColor;
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    
    //For BarC hart
    static NSNumberFormatter *barChartFormatter;
    if (!barChartFormatter){
        barChartFormatter = [[NSNumberFormatter alloc] init];
        barChartFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
        barChartFormatter.allowsFloats = NO;
        barChartFormatter.maximumFractionDigits = 0;
    }
    
    PNBarChart * barChart = [[PNBarChart alloc] initWithFrame:CGRectMake(0,  [UIScreen mainScreen].bounds.size.height-200, SCREEN_WIDTH, 200.0)];
    barChart.backgroundColor = [UIColor clearColor];
    
    barChart.yChartLabelWidth = 20.0;
    barChart.chartMarginLeft = 30.0;
    barChart.chartMarginRight = 10.0;
    barChart.chartMarginTop = 5.0;
    barChart.chartMarginBottom = 10.0;
    
    
    barChart.labelMarginTop = 5.0;
    barChart.showChartBorder = YES;
    [barChart setXLabels:@[@"NOV 13",@"NOV 14",@"NOV 15",@"NOV 16",@"NOV 17",@"NOV 18",@"NOV 19"]];
    barChart.yLabels = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10"];
    [barChart setYValues:@[@"7",@"5",@"6",@"6",@"8",@"7",@"1"]];
    [barChart setStrokeColors:@[PNGreen,PNGreen,PNGreen,PNGreen,PNGreen,PNGreen,PNRed]];
    barChart.isGradientShow = NO;
    barChart.isShowNumbers = NO;
    
    [barChart strokeChart];
    
    //barChart.delegate = self;
    
    [self.view addSubview:barChart];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:1 animated:YES];
//    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self displayAlarmTimeToLabel];
    
    self.ico_arrow.alpha = 1;
    
    //[self setStateIcoAlarm];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}



-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    //    AppDelegate *app=[UIApplication sharedApplication].delegate;
    //    [app.sliderVC setPanEnabled:NO];
}

#pragma mark -functional view method


-(void)displayAlarmTimeToLabel{
    
    TLCUserDefaultManager *userDef = [TLCUserDefaultManager sharedManager];
    
    NSDictionary *alarmTimeObj = [userDef getDefaultAlarmTime];
    NSString *setStr = [NSString stringWithFormat:@"%@:%@",alarmTimeObj[@"hour"],alarmTimeObj[@"minute"]];
    
    [self.alarmTimeButton setTitle:setStr forState:UIControlStateNormal];
    
   /*
    if([userDef getDefaultAlarm]){
        
        self.alarmTimeButtonWrapper.backgroundColor = gradientColor;
        
    } else {
        
        self.alarmTimeButtonWrapper.backgroundColor = gradientColor;
    }
    */
}

-(void)setStateIcoAlarm{
    if([[TLCUserDefaultManager sharedManager] getDefaultAlarm]){
        [[UIApplication sharedApplication] setStatusBarStyle:1 animated:YES];
    } else {
        [[UIApplication sharedApplication] setStatusBarStyle:0 animated:YES];
    }
    
}


- (IBAction)onTapToSettingButton:(UIButton *)sender {
    
    self.ico_arrow.alpha = 0.3;
    TLCDetailViewController *view = [[TLCDetailViewController alloc] init];
    
    [self.navigationController pushViewController:view animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
