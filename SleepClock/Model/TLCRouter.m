//
//  TLCRouter.m
//  SleepClock
//
//  Created by andy on 2016-11-18.
//
//

#import "TLCRouter.h"
#import "AppDelegate.h"
#import "TLCUserDefaultManager.h"
#import "TLCAlarmManager.h"

static TLCRouter *sharedManager=nil;

@implementation TLCRouter

+ (TLCRouter *)sharedManager{
    
    static dispatch_once_t once;
    dispatch_once( &once, ^{
        sharedManager=[[self alloc] init];
    });
    
    return sharedManager;
    
}

+ (id)allocWithZone:(NSZone *)zone {
    
    __block id ret = nil;
    
    static dispatch_once_t once;
    dispatch_once( &once, ^{
        sharedManager = [super allocWithZone:zone];
        ret           = sharedManager;
    });
    
    return  ret;
    
}

- (id)copyWithZone:(NSZone *)zone{
    
    return self;
    
}

#pragma mark - functional

-(void)gotoAwakeAlarmViewController:(BOOL)isAnimate{
    
    if( [UIApplication sharedApplication].applicationIconBadgeNumber > 0 ){
        
        if(![[TLCUserDefaultManager sharedManager] getDisplayedAwakeAlarmView] && ![[TLCAlarmManager sharedManager] isAdView]){
            
            UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
            
            //            UIViewController *nextVC =[self transportationListEnterpoint];
            UIViewController *nextVC = [topController.storyboard instantiateViewControllerWithIdentifier:@"CalcViewController"];
            
            [topController presentViewController:nextVC animated:isAnimate completion:nil];
        }
    }
}


-(void)displayAwakeAlarmView{
    [sharedManager gotoAwakeAlarmViewController:YES];
}
@end
