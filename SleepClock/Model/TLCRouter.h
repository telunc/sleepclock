//
//  TLCRouter.h
//  SleepClock
//
//  Created by andy on 2016-11-18.
//
//

#import <Foundation/Foundation.h>

@interface TLCRouter : NSObject

+(TLCRouter *)sharedManager;

-(void)gotoAwakeAlarmViewController:(BOOL)isAnimate;


-(void)displayAwakeAlarmView;

@end
