//
//  TLCTimeManager.h
//  SleepClock
//
//  Created by andy on 2016-11-19.
//
//

#import <Foundation/Foundation.h>

@interface TLCTimeManager : NSObject

+(TLCTimeManager *)sharedManager;

-(NSMutableDictionary *)getNowDate;
-(NSMutableDictionary *)getAssignDate:(NSDate *)assginDate;

-(NSDate *)createDateFromHM:(NSString *)hour assginMinute:(NSString *)minute;
-(NSDate *)createTrimDateFromNSDate:(NSDate *)assginDate;


-(NSDate *)convertAlarmDate:(NSDate *)assginDate;

@end
