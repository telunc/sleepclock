//
//  TLCAlarmManager.h
//  SleepClock
//
//  Created by andy on 2016-11-18.
//
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVFoundation.h>
#import "AppDelegate.h"
#import "TLCUserDefaultManager.h"
#import "TLCRouter.h"

@interface TLCAlarmManager : NSObject

+(TLCAlarmManager *)sharedManager;


-(void)setLocalNotification:(NSDate *)assginDate;
-(void)clearLocalNotification;

-(void)setBgm;
-(void)stopBgm;

-(BOOL)isAdView;
-(void)setAdView:(BOOL)isView;

@end
