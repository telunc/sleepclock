//
//  AppDelegate.h
//  SleepClock
//
//  Created by andy on 2016-11-18.
//
//

#import <UIKit/UIKit.h>
#import "TLCViewController.h"
#import "TLCUserDefaultManager.h"
#import "TLCAlarmManager.h"
#import "TLCRouter.h"
#import "TLCPlistManager.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+(NSString *)CUSTOM_NOTIFICATION_DID_ENTER_BACKGROUND;
+(NSString *)CUSTOM_NOTIFICATION_DID_BECOME_ACTIVE;
+(NSString *)CUSTOM_NOTIFICATION_DID_RECEIVE_LOCAL_NOTIFICATION_IN_ACTIVE;
+(NSString *)CUSTOM_NOTIFICATION_DID_RECEIVE_LOCAL_NOTIFICATION_STATE_ACTIVE;
+(NSString *)CUSTOM_NOTIFICATION_DID_FINISH_LAUNCHING_WITH_OPTIONS;

@end

